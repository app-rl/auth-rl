#!/bin/bash

VERSION="$1"
TAG_MESSAGE="Release $VERSION"
BRANCH="master"

# Create a new Git tag and push it to the repository
git checkout "$BRANCH" && git pull origin "$BRANCH"
git tag -a "$VERSION" -m "$TAG_MESSAGE"
git push origin "$VERSION"

echo "Created and pushed release $VERSION"