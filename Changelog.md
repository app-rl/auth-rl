
## v1.4.0

**END OF LIFE**


## v1.3.0

### Removed:
- Remove all of the `Auth` process from the package.
- Move all the migrations to the `rl-laravel-framework`.


## v1.2.5

### Modified:
- Remove token validation (to prevent error that cause the token to be invalid because of different JWT token).


## v1.2.4

### Fixes:
- Fix `activeToken` to work with the new OAuth2 if activated.
  - This will resolve problem with the permission update from the Account API.


## v1.2.3

### Modified:
- Change `isNewLogin` to `isActive` when checking if the `OAuth2` is active.


## v1.2.2

### Hotfix:
- Fix the permissions routes to work with the new OAuth2.
- Fix the `activeToken` to support also the new OAuth2 token (if new login is active).


## v1.2.1

### Added:
- Add `hasPermissionOrFail` function, ifn't found permission of user throw `PermissionException`.
- Add 'allowed' string, to permissions arrived to user from accounts-api (to spesific & childrens permissions, - not parents permission!).

### Fix:
- Fix `permission middelware` that check limmited permission on a route (if have many permissions middelwares: by group|route) 


## v1.2.0

### Deprecated:
- All of the `Auth` process is deprecated and will be removed soon.

### Removed:
- Remove `TelescopeAuth` middleware - Moved to the framework.


## v1.1.22

### Package:
- Added support to telescope 5.0 in the package require.


## v1.1.21

### Added:
- Add connection between Microsoft & Harmony accounts for single account.
- Access to accounts system in the state that the authUser is microsoft account and does not have an harmony_code.
- Add fullSlug to permission in permission-manager tool (include parents slug, implode by `:`).

### Fixes:
- Change save translates in DB (There is a difference in save translates from tool permissions | permissions.json)

## v1.1.20

### Fixes:
- In the state of deletion permission (Source|Feature...) to delete its association with the role, if it exists.

- Fix Edit & save translates in permission-manager tool.

### Delete:
- Delete associate_has_models table if exists.
- Delete IsCanAbility model - not in use.

## v1.1.19

### Fixes:
-  Fix save permission.json to database.
-  Fix associate permission to role by updateRoles.php

## v1.1.18

### Fixes:
- Fix path of childrens permission in tree permission.

## v1.1.17

### Added:
- Support for granting permissions to a role using pemission-manager the tool.
- support of granting permissions according to the parent permission.

### Changes
- Changing the names of tables and models to include a permission prefix instead of auth_

### Fixes:
- Prevent from the `Cache` class to flush the cache when the tag name is empty.
    - This will prevent from the whole cache to be flushed when the tag name is empty.

## v1.1.16

### Hotfix:
- Make static token in the activeToken so it will be shared between all the requests, the static token is also will be validated before using it in the request, else the token from the cache or the hook request will be used.


## v1.1.15

### Hotfix:
- Fix active token when the token is expired or not valid - the token will be refreshed and the request will be re-executed.


## v1.1.14

### Fixes:
- Fix update permissions & roles in the permissions manager route.
- Fix permissions manager assets path.


## v1.1.13

### Fixes:
- Send status code 401 when throw `AuthException`.

### Added:
- Added `Application::getEmployeesData` method to fetch employees data from the Account API.


## v1.1.11

### Changes:
- Temporary disable token `StrictValidAt` validation.


## v1.1.10

### Fixes:
- Fix error when fetching record from the `Account` if the query is containing `select` without the `account_uuid`.

## v1.1.9

### ENV:
- Set `PERMISSIONS_DISABLED` to disable the permissions from env - for development purposes.


## v1.1.6

### Fixes:
- Temporary fix to account_uuid is not matched to the uuid in the Account uuid - Force account update to use the correct uuid from the Account UUID.
