
# Rami Levi - Laravel Auth Package (Deprecated)
  
## About

The package takes care of verifying the token and is responsible for managing the application login system <br/>

The package includes an extension to the telescope.

## Installation
Install a package by command:
```shell
composer require app-rl/auth-rl:dev-master#v1.0
```
Put in composer.json in scripts section:
```json
"post-install-cmd": [
    "@php artisan permissions:reset", 
    "@php artisan permissions:update"
]
```
_**Please note**: permissions reset should come before update, in order to reset the permissions before updating them, In local environment the reset command will let the developer choose the reset method._

Run commands:

```shell
composer require doctrine/dbal
php artisan migrate
php artisan optimize:clear
```

Extend account model from the package in your Account.php model:
```php
use AuthRL\App\Models\Account as PackageAccount;

class Account extends PackageAccount {
    //
}
```

**Optional:** If you want to use different Account model, Add the model to `auth.php`:

The `User.php` is for the example, the default model is `Account.php`.
```php
'account_model' => App\Models\User::class
```

---
### Commands

Update permissions into Account API:
```
php artisan permissions:update
```

Reset permissions into the application DB from the local database.json:
```
php artisan permissions:reset
```

Clear permissions cache:
Use this command to clear cache after changing account permissions in the Account API.
Can be helpful in case of working on local environment where the Account API is in staging mode.
```
php artisan permissions:cache:clear
``` 
---

# Permissions

### Via `Middleware`

Add permission to route:
```
permissions: FirstPermission|SecondPermission|etc...

// For example:
permissions: Source1:Feature1:Section1:Ability1|Source2:Feature2:Section2:Ability2
```

```php
Route::group(['middleware' => ['permissions: FirstPermission|SecondPermission|etc...']], function() {
    //
});
```

### Via `Account` model
The package `Account` model is extending permissions trait:

Check if account has some permission(s):
```php
Account::find(1)->hasPermission('Source1:Feature1:Section1:Ability1');

Account::find(1)->hasPermission([
	'Source1:Feature1:Section1:Ability1',
	'Source2:Feature2:Section2:Ability2'
]);
```

Get account permissions tree:
```php
Account::find(1)->getPermissions();
```
This method also sync the account permissions from the Account API in case there is no cache for the account permissions in the current application.
